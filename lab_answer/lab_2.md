1. Apakah perbedaan antara JSON dan XML?

Terdapat beberapa perbedaan antara JSON dan XML, diantaranya adalah :

a. JSON merupakan standar berbasis terbuka berbasis teks untuk pertukuran data, sedangkan XML merupakan format independen perangkat lunak-perangkat keras untuk pertukuran data.

b. JSON merupakan bahasa meta, sedangkan XML merupakan bahasa markup.

c. Kompleksitas JSON lebih sederhana, sedangkan XML lebih rumit.

d. JSON berorientasi pada data, sedangkan XML berorientasi pada dokumen.

e. JSON mendukung array, sedangkan XML tidak.

2. Apakah perbedaan antara HTML dan XML?

Terdapat beberapa perbedaan antara HTML dan XML, diantaranya adalah :

a. XML berfokus pada transfer data, sedangkan HTML berfokus pada penyajian data.

b. Tag pada XML dapat dikembangkan, sedangkan HTML memiliki tag yang terbatas.

c. Tag XML tidak ditentukan sebelumnya, sedangkan HTML memiliki tag yang telah ditentukan sebelumnya.

d. XML didorong oleh konten, sedangkan HTML didorong oleh format.

e. XML memperhatikan Case Sensitive, sedangkan HTML tidak.

f. XML sangat ketat dalam memperhatikan tag penutup, sedangkan HTML tidak.
