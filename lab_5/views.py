from django.shortcuts import render
from lab_2.models import Note

def index(request):
    data = Note.objects.all()
    response = {'data': data}
    return render(request, 'lab5_index.html',response)

