import 'package:flutter/material.dart';
import 'package:lab_6/models/models.dart';
import 'package:lab_6/Views/ArticleCard.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xff212529),
        appBar: AppBar(
          backgroundColor: Color(0xff212529),
          title: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Text(
              'INVID 19',
              style: TextStyle(color: Colors.white),
            ),
            SizedBox(width: 10)
          ]),
        ),
        body: Column(children: <Widget>[
          Row(children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(15),
              child: Text("ARTIKEL",
                  style: TextStyle(color: Colors.white, fontSize: 20)),
            ),
            Spacer(),
            Padding(
              padding: const EdgeInsets.only(right: 10),
              child: SizedBox(
                //constraints: BoxConstraints.tightFor(width: 150, height: 40),
                width: 200,
                child: TextField(
                  decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.blue, width: 3)),
                      hintText: 'Cari Artikel'),
                ),
              ),
            ),
          ]),
          Expanded(
            child: ListView.builder(
                itemCount: Dummy_Article.length,
                itemBuilder: (context, index) {
                  return ArticleCard(context, Dummy_Article[index]);
                }),
          )
        ]));
  }
}
