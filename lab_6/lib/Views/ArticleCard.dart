import 'package:flutter/cupertino.dart';
import 'package:lab_6/models/models.dart';
import 'package:flutter/material.dart';

Widget ArticleCard(BuildContext context, Article article) {
  return Padding(
      padding: const EdgeInsets.all(10),
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              leading: Hero(
                  tag: article.images, child: Image.network(article.images)),
              title: Text(article.judul),
              subtitle: Text(article.Deskripsi),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(article.tanggal),
                const SizedBox(width: 8),
                TextButton(
                  child: const Text('BACA ARTIKEL'),
                  onPressed: () {/* ... */},
                ),
                const SizedBox(width: 8),
              ],
            ),
          ],
        ),
      ));
}
