class Article {
  final String judul;
  final String Deskripsi;
  final String tanggal;
  final String images;

  const Article(
      {required this.judul,
      required this.Deskripsi,
      required this.tanggal,
      required this.images});
}

const Dummy_Article = const [
  Article(
      judul: "Vaksinasi Diundur, Ini Kabar Uji Klinis Vaksin Corona Bandung",
      Deskripsi: "Deskripsi Singkat Artikel",
      tanggal: "10 November 2020",
      images:
          "https://d1bpj0tv6vfxyp.cloudfront.net/articles/331057_15-1-2021_13-34-9.webp"),
  Article(
      judul: "Makanan yang Ampuh Menurunkan Kadar Gula Darah",
      Deskripsi: "Deskripsi Singkat Artikel",
      tanggal: "12 November 2021",
      images:
          "https://d1bpj0tv6vfxyp.cloudfront.net/articles/1100_18-11-2020_15-39-47.webp"),
  Article(
      judul: "Waspada Gejala Pneumonia, Penyakit Paru Berbahaya",
      Deskripsi: "Deskripsi Singkat Artikel",
      tanggal: "12 November 2021",
      images:
          "https://d1bpj0tv6vfxyp.cloudfront.net/articles/8c52c040-522b-40d9-8492-3ae6e0ffa3f8_article_image_url.webp"),
];
