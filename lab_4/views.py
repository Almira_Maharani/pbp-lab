from django.http import HttpResponseRedirect
from django.shortcuts import render
from lab_2.models import Note
from lab_4.forms import NoteForm

def index(request):
    data = Note.objects.all()
    response = {'data': data}
    return render(request, 'lab4_index.html',response)

def add_note(request):
    form = NoteForm(request.POST)
    if(form.is_valid() and request.method=="POST"):
        form.save()
        return HttpResponseRedirect("/lab-4")
    response = {'form':form}
    return render(request, 'lab4_form.html',response) 

def note_list(request):
    note = Note.objects.all()
    response = {'note':note}
    return render(request,'lab4_note_list.html',response)

