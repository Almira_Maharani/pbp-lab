import 'package:flutter/material.dart';
import 'package:lab_7/Models/models.dart';
import 'package:lab_7/Views/ArticleCard.dart';
import 'package:lab_7/AddArticle.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xff212529),
        appBar: AppBar(
          backgroundColor: Color(0xff212529),
          title: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Text(
              'INVID 19',
              style: TextStyle(color: Colors.white),
            ),
            SizedBox(width: 10)
          ]),
        ),
        body: Column(children: <Widget>[
          Row(children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(15),
              child: Text("ARTIKEL",
                  style: TextStyle(color: Colors.white, fontSize: 20)),
            ),
            Spacer(),
            Padding(
                padding: const EdgeInsets.only(right: 10),
                child: ElevatedButton(
                  child: const Text('Tambah Artikel'),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const AddArticle()));
                  },
                )),
          ]),
          Expanded(
            child: ListView.builder(
                itemCount: Dummy_Article.length,
                itemBuilder: (context, index) {
                  return ArticleCard(context, Dummy_Article[index]);
                }),
          )
        ]));
  }
}
