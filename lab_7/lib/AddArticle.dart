import 'dart:html';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab_7/main.dart';

class AddArticle extends StatelessWidget {
  const AddArticle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Color(0xff212529),
          title: const Text('Tambah Artikel',
              style: TextStyle(color: Colors.white)),
        ),
        body: Center(
          child: ArticleForm(),
        ));
  }
}

class ArticleForm extends StatelessWidget {
  ArticleForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
          child: TextFormField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Judul Artikel',
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8, vertical: 10),
          child: SizedBox(
            width: 600,
            child: TextFormField(
              maxLines: 8,
              style: TextStyle(fontSize: 15, height: 1.5),
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Isi Artikel',
              ),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8, vertical: 10),
          child: SizedBox(
            child: TextFormField(
              style: TextStyle(fontSize: 15, height: 1.5),
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Peninjau Artikel',
              ),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8, vertical: 10),
          child: SizedBox(
            child: TextFormField(
              style: TextStyle(fontSize: 15, height: 1.5),
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Thumbnail',
              ),
            ),
          ),
        ),
        Row(
          children: <Widget>[
            Padding(
                padding: const EdgeInsets.all(15),
                child: ElevatedButton(
                  child: const Text('Kembali'),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                )),
            Spacer(),
            Padding(
                padding: const EdgeInsets.all(15),
                child: ElevatedButton(
                  child: const Text('Tambah'),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ))
          ],
        )
      ],
    );
  }
}
